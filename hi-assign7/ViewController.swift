//
//  ViewController.swift
//  hi-assign7
//
//  Created by Student on 2016-11-09.
//  Copyright © 2016 Student. All rights reserved.
//
//  Project:      assign-7-2016
//  File:         ViewController.swift
//  Author:       Ismael F. Hepp
//  Date:         Nov 15, 2016
//  Course:       IMG204
//
//  You are to develop a Single View iOS application that should satisfy the
//  following requirements:
//
//  1 - Look at the very incomplete exploratory app (zip file) available at
//  /img204/solutions/Assignment7 .
//  2 - he application is a game to guess a sum of two dice. You play against
//  the computer.
//  3 - Use labels and text fields to show the sum you guess. After you make
//  your guess, the computer will show its own guess (it must be different from
//  yours).
//  4 - Click on a Play button to roll both dice (use random number generator).
//  The result of the roll should be displayed using both images and the number
//  representing their sum. A point is scored if you or the computer correctly
//  predicted the result.
//  5 - Repeat step 4 as many times as you like.
//  6 - At any moment you can repeat step 3.
//  7 - There should be set of labels to display how many times both you and
//  the computer guessed correctly the result.
//  8 - Use StackView to simplify the layout of your app.
//  9 - Create a group called Model.
//  10 - Inside the Model group, create necessary classes to play the game
//  (your previous assignment dealing with dice should help) and to maintain
//  the score.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

  
  @IBOutlet weak var imageView1: UIImageView!
  @IBOutlet weak var imageView2: UIImageView!
  @IBOutlet weak var pcScore: UILabel!
  @IBOutlet weak var playerScore: UILabel!
  @IBOutlet weak var playerGuess: UITextField!
  @IBOutlet weak var pcGuess: UITextField!
  
  
  // Model
  var dice : PairOfDice = PairOfDice()
  
  
  // Other variables
  var currentPlayerGuess: Int = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    // Connect delegate for the playerGuess and pcGuess
    playerGuess.delegate = self
    //pcGuess.delegate = self
    
    // Load dice images
    imageView1.image = UIImage(named: String(dice.showDie1()))
    imageView2.image = UIImage(named: String(dice.showDie2()))
    
    // Load initial score
    pcScore.text = "0"
    playerScore.text = "0"
    
    // Initial PC guess
    pcGuess.text = String(Int(arc4random_uniform(10)) + 2)
    currentPlayerGuess = 0
    
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  @IBAction func buttonPlay(_ sender: AnyObject) {
    
    if let intTest: Int = Int(playerGuess.text!) {
    } else {
      return
    }
    
    if currentPlayerGuess != Int(playerGuess.text!)! {
      randomPCGuess()
      currentPlayerGuess = Int(playerGuess.text!)!
      
    }
    
    if currentPlayerGuess < 2 || currentPlayerGuess > 12 {
      playerGuess.text = ""
      playerGuess.placeholder = "2-12"
      self.playerGuess.becomeFirstResponder()
      return
    }
    
    dice.rollDice()
    imageView1.image = UIImage(named: String(dice.showDie1()))
    imageView2.image = UIImage(named: String(dice.showDie2()))
    
    
    if Int(playerGuess.text!)! == dice.sumOfDice {
      playerScore.text = String(Int(playerScore.text!)! + 1)
    }
    
    if Int(pcGuess.text!)! == dice.sumOfDice {
      pcScore.text = String(Int(pcScore.text!)! + 1)
    }
    
    

  }
  
  /////////   TEXTFIELD  SECTION ///////////
  // Implementation of the delegate for textFieldInt
  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool
  {
    let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
    
    if string.rangeOfCharacter(
      from: invalidCharacters,
      options: [],
      range: string.startIndex ..< string.endIndex) == nil {
      return true
    }
    
    return false
    
  }
  
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    var guess : Int
    
    repeat {
      guess = Int(arc4random_uniform(10)) + 2
    } while (guess == Int(playerGuess.text!))
    
    pcGuess.text = String(guess)
  }
  
  func randomPCGuess() {
    var guess : Int
    
    repeat {
      guess = Int(arc4random_uniform(10)) + 2
    } while (guess == Int(playerGuess.text!))
    
    pcGuess.text = String(guess)
  }


}

